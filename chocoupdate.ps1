git pull
$ErrorActionPreference = "Stop"
if ((Get-WmiObject -Class BatteryStatus -Namespace root\wmi ).PowerOnLine) {
    New-BurntToastNotification -Text "Script updates", "On Line"
}
if (!(Import-Module -Name BurntToast)) {
    Install-Module -Name BurntToast -Force;
    Import-Module -Name BurntToast
}

$outdated = (choco outdated -r | Select-String '^([^|]+)\|.*$').Matches.Groups | Where-Object {$_.Name -eq 1} | ForEach-Object {$_.Value}
$pretty = ($outdated -join ', ')

If ($outdated.count -gt 0) {

	cup -y all

	$stilloutdated = (choco outdated -r | Select-String '^([^|]+)\|.*$').Matches.Groups | Where-Object {$_.Name -eq 1} | ForEach-Object {$_.Value}
If ($stilloutdated.count -gt 0) {

	$stillpretty = ($stilloutdated -join ', ')
	$stillpretty = ", " + $stillpretty + " needing update"
}
	$counted = $outdated.count - $stilloutdated.count

	New-BurntToastNotification -Text "Updated/Outdated chocolatey packages", "$counted updated $stillpretty"
}
shutdown -s -t 3600
