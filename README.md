# Choco update

Scripts pour la mise à jour automatique des paquets choco

Script to handle the automatic update of the choco packages

This script should give you a popup with the number of packets updated and the name of them.

If there are some packets that aren't updating even if there is a new choco package, it will tell you.

You need to add a scheduled task to automate this or run it manualy.